--[[

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

]]--


function split(s, sep)
	local t = {}
	if not s then return t end
	local component = ""
	for i = 0, #s do
		local c = string.sub(s, i, i)
		if c == sep then
			table.insert(t, component)
			component = ""
		else
			component = component .. c
		end
	end
	table.insert(t, component)
	return t
end
if unit_test then
	assert(table.concat(split("this.is.a.path", ".")) == "thisisapath")
	assert(table.concat(split("this", ".")) == "this")
	assert(table.concat(split("", ".")) == "")
end


function ipairs_rev(array)
	local iter = function(a, i)
		i = i - 1
		if i < 1 then return nil end
		return i, array[i]
	end
	return iter, array, #array + 1
end


function count_table(table)
	count = 0
	for _ in pairs(table) do
		count = count + 1
	end
	return count
end


function indent(level)
    local s = ""
	for x = 1,level do
	    s = s .. "\t"
	end
    return s
end


function print_table(title, table, log_fn, level)
    log_fn = log_fn or function(...) io.write(...); io.write("\n") end
	level = level or 0
	log_fn(indent(level), title, "(", count_table(table),  ")");
	for k, v in pairs(table) do
		if type(v) == "table" then
			print_table(k, v, log_fn, level + 1)
		else
			log_fn(indent(level + 1), k, " = ", tostring(v))
		end
	end
end


function table.shallow_copy(org)
	local new = {}
	for k, v in pairs(org) do
		new[k] = v
	end
	return new
end


function table.count(t)
  local count = 0
  for _ in pairs(t) do count = count + 1 end
  return count
end


function using(t)
	for k, v in pairs(t) do
		if _G[k] then
			error("Conflicting symbol")
		end
		print("adding:", k, v)
		_G[k] = v
	end
end

