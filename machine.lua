--[[

Copyright (c) 2020 Jason Spashett

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

]]--


require 'utility'
require 'log'


local function parent(state_name)
	if not state_name then return "" end
	for i = #state_name,1,-1 do
		if state_name:sub(i,i) == "." then
			return state_name:sub(1, i - 1)
		end
	end
	return "" 
end
if unit_test then
	assert(parent(nil) == "")
	assert(parent("this.is.a.path") == "this.is.a")
	assert(parent("this") == "")
	assert(parent("this.") == "this")
	assert(parent("this.is.") == "this.is")
	assert(parent("") == "")
	assert(parent(".") == "")
end


local function path_as_list(path)
	return split(path, ".")
end


local function lca_for_path(path1, path2)
	local lca = ""
	local t1 = path_as_list(path1)
	local t2 = path_as_list(path2)
	local i1 = 1
	local i2 = 1
	while t1[i1] and t2[i2] do
		if t1[i1] ~= t2[i2] then
			break
		end
		lca = lca .. t1[i1] .. "."
		i1 = i1 + 1
		i2 = i2 + 1
	end
	lca = lca:sub(1, #lca -1)
	return lca
end
if unit_test then
	assert(lca_for_path("jason.was.here.before", "jason.was") == "jason.was")
	assert(lca_for_path("jason.is.here.before", "jason.was") == "jason")
	assert(lca_for_path("jason", "jason") == "jason")
	assert(lca_for_path("root", "") == "")
end


local function route_between_paths(source, dest)
	local visits = {}
	local lca = lca_for_path(source, dest)
	local x = source
	while x ~= lca do
		table.insert(visits, x)
		x = parent(x)
	end
	return visits
end
if unit_test then
	assert(table.concat(
		route_between_paths("machine_up.active.main_menu",
		"machine_down"), "|") == "machine_up.active.main_menu|" ..
		"machine_up.active|machine_up")
end


local function join_path(base, rel)
	if not base then return rel end
	local path = base
	if #path > 0 then
		path = path .. "."
	end
	path = path .. rel
	return path
end
if unit_test then
	assert(join_path("", "fish") == "fish")
	assert(join_path("root", "sub") == "root.sub")
end


-- convert relative name to absolute name
local function to_absolute_name(base, target)
	if not target then return target end
	base = base or ""
	local abs
	local sigil = string.sub(target, 1, 1)
	if sigil == "<" then
		abs = join_path(parent(base),  target:sub(2))
	elseif sigil == ">" then 
		abs = base
		abs = join_path(abs, target:sub(2))
	else
		abs = target
	end
	--[[
    if target ~= abs then
        self.debug("converting relative " .. target .. " to " .. abs .. 
		" against a base of '" .. base .. "'")
    end
	]]--
	return abs 
end
if unit_test then
	assert(to_absolute_name("base.path", "new.path") == "new.path")
	assert(to_absolute_name("base.path", "<name") == "base.name")
	assert(to_absolute_name("base.path", ">cat") == "base.path.cat")
	assert(to_absolute_name("", ">cat") == "cat")
	assert(to_absolute_name(nil, ">cat") == "cat")
	assert(to_absolute_name(nil, "<cat") == "cat")
	assert(to_absolute_name(nil, "cat") == "cat")
end


-- TODO This piece is slow
local function find_substates(chart, parent)
	local substates = {}
	local substate_prefix = parent .. "."	
	for k, v in pairs(chart) do
		if k:find(substate_prefix) == 1 then
			table.insert(substates, k)
		end
	end
	return substates
end


if unit_test then
	local machine = 
		{ 
			["animal"] = {},
			["animal.bear" ] = {},
			["animal.dog" ] = {},
			["ant"] = {}
		}
	local substates = find_substates(machine, "animal");
	table.sort(substates);
	--[[
	print_table("substates", substates, debug)
	]]--
	assert( 
		table.concat(substates, "&") == "animal.bear&animal.dog"
	)
	assert(#find_substates(machine, "ant") == 0);
end


Machine = {}


function Machine:check_chart()
	for k, _ in pairs(self.chart) do
		local p = parent(k)
		if p ~= "" then
			-- Parent check
			if not self.chart[p] then
				error("check_chart() '" .. k .. "' has no parent, but is " ..
					"defined as a sub-state")
			end
			-- Initial state check (this check might not be quite right)
			local initial = self.chart[p].initial
			local concurrent = self.chart[p].concurrent_substates
			if not (initial or concurrent) then
				error("check_chart() parent of '" .. k .. "', which is '" ..
					p .. "' does not define an initial state, or declare " ..
					"that its sub-states are concurrent.")
			end
		end
	end
end


function Machine:new(chart, options)
	local new = {}
	options = options or {}
	setmetatable(new, {__index = self})
	self = new
	self.chart = chart
	self:check_chart()
	self.current = nil
	self.log = options["log"] or function() end
	self.info = options["info"] or self.log
	self.debug = options["debug"] or self.log
	self.info("Create new machine")
	return self
end


function Machine:notify(fn)
	self.notify_fn = fn
end


function Machine:exit_state(name)
	local t = self:find_transition(name, "_exit")
	if t then self:do_action(name, t.action) end
	local substates = find_substates(self.current, name)
	for _, s in ipairs(substates) do
		self.debug("found state to exit ", s)
		self:exit_state(s)
	end
	self.current[name] = nil
end


function Machine:enter_state(current, new_state)
	if not new_state then 
		error("target new_state is nil")
	end
	
	new_state = to_absolute_name(current, new_state)


	self.debug("enter_state: " .. current .. " -> " .. new_state)

	local new_state_node = self.chart[new_state]
	if not new_state_node then
		error("Could not find new_state " .. new_state)
	end

	-- EXIT STATE
	-- Call exit handlers while unwinding
	local exit_routes = route_between_paths(current, new_state)
	print_table("exit_routes for " .. current .. " -> " .. new_state,
		exit_routes, self.debug)	
	for _, s in ipairs(exit_routes) do
		-- Find child states and exit them also
		self:exit_state(s)
	end
	
	-- call entry handlers when diving
	for _, s in ipairs_rev(route_between_paths(new_state, current)) do
		local t = self:find_transition(new_state, "_enter")
		if t then self:do_action(s, t.action) end
	end

	self.current[new_state] = true

	local initial = to_absolute_name(new_state, new_state_node.initial)
	if initial then
		self.debug("In state " ..  new_state .. " setting initial state to " 
			.. initial)
		self:enter_state(new_state, initial)
	end

	-- Orthogonal states
	local orthagonal = self.chart[new_state].concurrent_substates
	if orthagonal then
		self.debug("The state ", new_state, " is orthogonal")
		local substates = find_substates(self.chart, new_state)
		for _, s in ipairs(substates) do
			self.debug("found orthogonal state ", s)
			self.current[s] = true
			self:enter_state(new_state, s)
		end
	end
	
end




function Machine:find_transition(state_name, evt)
	local state_node = assert(self.chart[state_name])
	if not state_node then return nil end
	local transitions = state_node.transitions
	if transitions then 
		local t = transitions[evt]
		if t then
			self.debug("Found transition for " .. tostring(evt) .. 
				" in state " .. state_name)
            local guard = t.guard
            if guard then
                self.debug("\tChecking guard " .. guard)
                local guard_result = self:do_guard(self.current, guard)
                if not guard_result then
                    self.debug("\tGuard inhibited the transition by " ..
					"returning " .. tostring(guard_result) )
                    return nil
                end
            end
			return t
		end
	end
end


function Machine:do_notify(params)
    if not self.notify_fn then
        warn("No notify function to handle machine message, message follows:")
        print_table("params", params, warn)
        return
    end
	return self.notify_fn(params)
end


NotifyType = { Action = 0, Guard = 1 }


function Machine:do_action(state, action)
	return self:do_notify{ type = NotifyType.Action, state = state, 
        action = action }
end


function Machine:do_guard(state, guard)
	return self:do_notify{ type = NotifyType.Guard, state = state, 
        guard = guard }
end


function Machine:transition(current, evt)
	local state_node = self.chart[current]
	if not state_node then return end

	local t
	repeat
		t = self:find_transition(current, evt)
		if t then break end
		local p = parent(state_name) 
		state_node = self.chart[p]
	until not state_node
	if not t then
		if evt and evt:sub(1,1) ~= '_' then 
			self.debug("No valid transition found for " .. evt .. 
				" in state " .. current)
		end
		return
	end

	local s = t.state
	if s then 
		-- Found state target
		self.debug("Found transition target to " .. s .. " in state " .. 
			current)
		self:enter_state(current, s)
	end
	if t.action then
		self:do_action(self.current, t.action)
	end

end


function Machine:event(evt)
	self.debug("Machine:event('" .. tostring(evt) .. "')")
	if self.current then
		print_table("states", self.current, self.debug)
	end

	if not self.current then -- This starts the machine
		self.current = {}
		self.info("Starting machine")
		self:enter_state("", self.chart.initial)
	end

	local otable = table.shallow_copy(self.current)
	print_table("otable", otable, self.debug)
	for s, _ in pairs(otable) do
		self:transition(s, evt)
	end
end

