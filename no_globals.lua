
function define_global(name, value)
	print("setting", name)
	rawset(_G, name, value or 0)
end

local g_meta = {
	__newindex = function(table, name, value)
--		if type(value) ~= "function" then
			print("Index", table, name, value)
			error("Use define_global to add a global")
--		else
			rawset(table, name, value)
		end
--	end
}
setmetatable(_G, g_meta)

