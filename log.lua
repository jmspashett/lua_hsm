log_levels = { 
	info = "info",
	warn = "warn",
	debug = "debug"
}

function log(level, ...)
	io.write("log: ", level, ": ", ...)
	io.write("\n")
end

function info(...)
	log(log_levels.info, ...)
end


function warn(...)
	log(log_levels.warn, ...)
end

function debug(...)
	log(log_levels.debug, ...)
end

