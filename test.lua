#!/usr/bin/env lua

--[[
Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

test_machine = {

	initial = "shutdown",

	-- Main system state
	["idle"] = { 
	},

	["shutdown"] = {
		transitions = 
		{
			_enter = {
				action = "action_log_entered_shutdown"
			},
			_exit = {
				action = "action_log_exit_shutdown"
			},
			evt_start = {
				state = "running"
			}
		}
	},
	["running"] = {
		concurrent_substates = true,
		--initial = "running.main_menu",
		transitions = {
			_enter = {
				action = "action_enter_running"
			},
			_exit = {
				action = "action_exit_running"
			},
			evt_shutdown = {
				state = "shutdown", 
				action = "action_system_shutdown"
			}
		}
	},

	["running.main_menu"] = {
		transitions = {
			_enter = {
				action = "action_log_enter_main_menu"
			},
			_exit = {
				action = "action_log_exit_main_menu"
			}
		}
	},
	["running.control"] = {
		transitions = {
			_enter = {
				action = "action_log_enter_control"
			},
			_exit = {
				action = "action_log_exit_control"
			}
		}
	}

}

require 'machine'
require 'log'


local results = {
	{action_log_entered_shutdown = true},
	{action_log_exit_shutdown = true},
	{action_enter_running = true},
	{action_log_enter_main_menu = true, action_log_enter_control = true},
	{action_exit_running = true},
	{action_log_exit_main_menu = true, action_log_exit_control = true},
	{action_log_entered_shutdown = true},
	{action_system_shutdown = true}
}

local m = Machine:new(test_machine)

m:notify(
	function(params)
        local notify_type = params['type']
        if notify_type == NotifyType.Action then
            local state = params['state']
            local action = params['action']
--[[
            info(string.format("*\tAction: state = %s, action = %s", 
                state, tostring(action)))
]]--

			if #results < 1 then
				table.insert(results, {"+" .. tostring(action)})
			elseif results[1][action] then
				results[1][action] = nil
				if table.count(results[1]) == 0 then
					table.remove(results, 1)
				end
			else
				table.insert(results, {"+" .. tostring(action)})
			end

        elseif notify_type == NotifyType.Guard then
            local guard = params['guard']
--[[
            info("*\tGuard: " .. tostring(guard))
]]--
            if guard == "guard_no_fault" then
                --info("*\tReturning true")
                return true
            end
        end
	end
)
 
local events = {
	"evt_start",
	"evt_shutdown"
}

debug = function() end

debug("running test")

m:event()
for i, e in ipairs(events) do
	debug("Sending event number ", i, " = ", e)
	m:event(e)
end
if #results > 0 then
	print_table("results", results, debug)
end
assert(#results == 0)

